
//validate register form
var checkRegister = function(req){
  console.log("ddddddddd");
  //TODO: check if email is also unique
  //TODO: check password strength
  req.checkBody('email', 'email is required and can\'t be empty' ).notEmpty();
  req.checkBody('email','invalid email').isEmail();
  req.checkBody('email', 'email address must be between 4-100 charechters ').len(4,100);
  req.checkBody('password','password is required and can\'t be empty').notEmpty();
  req.checkBody('password','password must be between 8-100 charechters').len(8,100);
  /*
    ^                         Start anchor
    (?=.*[A-Z].*[A-Z])        Ensure string has two uppercase letters.
    (?=.*[!@#$&*])            Ensure string has one special case letter.
    (?=.*[0-9].*[0-9])        Ensure string has two digits.
    (?=.*[a-z].*[a-z].*[a-z]) Ensure string has three lowercase letters.
    .{8}                      Ensure string is of length 8.
    $                         End anchor.
  */
  //req.checkBody('password','password must have two uppercase letters.').matches(/^(?=.*[A-Z].*[A-Z]) $/,"i");
  //req.checkBody('password','password must have one special case letter').matches(/^(?=.*[!@#$&*])$/,"i");
  //req.checkBody('password','password must have two digits').matches(/^(?=.*[0-9].*[0-9])$/,"i");
  //req.checkBody('password','password must have three lowercase letters.').matches(/^(?=.*[a-z].*[a-z].*[a-z])$/,"i");
//  req.checkBody('password','password must include one lowercase charechter, one uppercase charecter, a number, and a specialcharecter.').matches(/^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/);
  req.checkBody('passwordMatch','password is required and can\'t be empty').notEmpty();
  req.checkBody('passwordMatch', 'password do not match, please try again').equals(req.body.password);

  return req.validationErrors();
};

module.exports = {
  checkRegister: checkRegister
}
