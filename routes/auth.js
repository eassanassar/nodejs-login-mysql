var auth = require('../controllers/authCtrl');
var globals = require('../helpers/globals');
var passport = require('passport');


module.exports = function(app){
  app.get('/', function(req, res){ auth.index(req, res)});
  app.get('/register',function(req, res){   auth.registerView(req, res); });
  app.post('/register',  function(req, res){  auth.register(req, res);});
  app.get('/login', function(req, res){ auth.loginView(req, res)});
  app.post('/login',  auth.loginAuth);
  app.get('/profile', globals.isLoggedIn, function(req, res){ auth.profileView(req, res)});
  app.get('/logout', function(req, res){ auth.logout(req, res)});
}



passport.serializeUser(function(user, done){
  done(null, user);
});
passport.deserializeUser(function(user, done){
  done(null, user);
});
