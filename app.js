require('dotenv').config()
var express = require('express');
var authRoute = require('./routes/auth');
var loginValidate = require('./controllers/authCtrl').loginValidate;
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
//Authintication Packages
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
//when using mysql only with sessions(if diffrent database then check expression-session npm for it )
// a session table would be created automaticly in the database
// the session will stay untill we logout
var mySQLStore = require('express-mysql-session')(session);

var options = {
  host     : process.env.DB_HOST,
  user     : process.env.DB_USER,
  password : process.env.DB_PASS,
  database : process.env.DB_NAME
};

var sessionStore = new mySQLStore(options);


var app = express();


//set up template engine
app.set('view engine', 'ejs');
//app.set('views',  __dirname +'\\views');

//static files
app.use(express.static('./public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());

app.use(session({
    secret: 'a4f8071f-c873-4447-8ee2',
    //save even if unchanged
    resave: false,
    saveUninitialized: false,
    store: sessionStore
    //cookie: { maxAge: 2628000000 },

}));

app.use(passport.initialize());
app.use(passport.session());


app.use(function(req, res, next){
  //storing variables for the specific request/response cycle. in res.locals the variables are only available to the view associated with the response.
  res.locals.isLoggedIn = req.isAuthenticated();

//  console.log(res.status(404));
  next();
});


//fire routes
authRoute(app);

// Handle 404 - must Keep this as a last route
app.use(function(req, res, next) {
    res.status(404);
    res.redirect('/');
});




passport.use(new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password'
  },
 function(username, password, done) {
      loginValidate(username, password, done);
  }
));

app.listen('3000');
console.log("app is listening to port 3000");
