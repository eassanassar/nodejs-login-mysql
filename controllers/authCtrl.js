var authHelper = require('../helpers/auth');
var db = require('../db.js');
var passport = require('passport');


// for encryption use
var bcrypt = require('bcrypt');
const saltRounds = 10;

const registerViewPath = 'auth/register';
const loginViewPath = 'auth/login';
const indexPath ='home.ejs';
const profileViewPath = 'user/profile';



var registerView = function(req,res){
   res.render(registerViewPath, {title: 'Register'});
};
var loginView = function(req,res){
   res.render(loginViewPath);
};
var index = function(req,res){
  //console.log(req.user);
  //console.log(req.isAuthenticated());
   res.render(indexPath);
};
var profileView = function(req,res){
   res.render(profileViewPath);
};




var register = function(req, res){

  //validate register form
  var errors = authHelper.checkRegister(req);
  if(errors){
   console.log("errors", errors);
    res.render(registerViewPath, {title: 'Register Errors', errors: errors});
  }else{
    const email = req.body.email;
    const password = req.body.password;

    bcrypt.hash(password, saltRounds, function(err, hash){
      if(err) throw err;

      db.query('INSERT INTO users (email,password) VALUES (?, ?)', [email, hash], function(error, results, fields){
        if(error) throw error;

        db.query('Select LAST_INSERT_ID() as id',function(error,result,fields){
          if(error) throw error;
          var userId = result[0];
          console.log("user Id: ",userId);
          req.login(userId,function(err){
            res.render(indexPath);

          });
        });

      });
    });

  }
};

//if pass redirect to /profile, if nor go to /login
var loginAuth =  passport.authenticate('local',
  {
    successRedirect: '/profile',
    failureRedirect: '/login'
  }
);

// validate if pass or not
//gets called from the app.js in pasport.use(...)
var loginValidate = function(email, password, done){
  db.query('SELECT id, password FROM users WHERE email = ?',[email], function(err, results, fields){
    if(err) {
      done(err);
    }else {
      if(results.length === 0){
        done(null,false);
      }else{
        var hash = results[0].password.toString();
        var userId = results[0].id;
        bcrypt.compare(password,hash,function(err, response){
          if(response === true){
            return done(null,{userId: userId});
          }else{
            //false means not verefied
            //TODO: if sign in failed after few time , block him for few minutes
            return done(null, false);
          }
        });
      }
    }
  });
};

var logout = function(req, res){
  req.logout();
  req.session.destroy(() => {
       res.clearCookie('connect.sid')
       res.redirect('/')
   })
};



module.exports = {
    registerView : registerView,
    register: register,
    loginView: loginView,
    loginAuth: loginAuth,
    loginValidate: loginValidate,
    index: index,
    profileView: profileView,
    logout: logout,
  }
